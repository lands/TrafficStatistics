//
// Created by root on 17-3-1.
//

#ifndef TRAFFICSTATISTICS_TRAFFIC_DATA_PROCESS_H
#define TRAFFICSTATISTICS_TRAFFIC_DATA_PROCESS_H

#include <iostream>
#include <vector>
#include <map>
#include <SQLiteCpp/SQLiteCpp.h>

class TrafficProcess {
public:
    enum TRAFFIC_T {
        TRAFFIC_UPLOAD =0,
        TRAFFIC_DOWNLOAD
    };
    enum BLOCK_T {
        BLOCK_TARGET = 0,
        BLOCK_INITIAL_TIME,
        BLOCK_START_TIME,
        BLOCK_END_TIME,
        BLOCK_DATA
    };

public:
    typedef struct yaml_config {
        std::string db_file;
        std::string data_file;
        uint32_t collect_interval;
        uint32_t dump_at_minute;
        uint32_t dump_at_hour;
    } Yaml_Config_ST;
    typedef struct load_data {
        uint64_t upload;
        uint64_t download;
    } Load_Data_ST;
    typedef std::map<uint64_t, Load_Data_ST> Data_Map;
    typedef struct traffic_data {
        std::string target;
        TRAFFIC_T data_type;
        uint64_t initial_time;
        uint64_t start_time;
        uint64_t end_time;
        Data_Map data_map;
    } Traffic_Data_ST;
    typedef std::map<std::string, Traffic_Data_ST> Traffic_Data_Map;

    TrafficProcess();
    ~TrafficProcess();

    void set_yaml_config(Yaml_Config_ST config_st);
    Yaml_Config_ST get_yaml_config();
    void set_traffic_data_map(Traffic_Data_ST data_st);
    Traffic_Data_Map get_traffic_data_map();

    void data_parse(std::string filename);
    void data_to_memory();
    void data_to_temp();
    void data_store_by_hour();
    void data_store_by_day();

private:
    Yaml_Config_ST _yaml_config_st;
    Traffic_Data_ST _traffic_data_st;
    Traffic_Data_Map _traffic_data_map;
};

#endif //TRAFFICSTATISTICS_TRAFFIC_DATA_PROCESS_H
