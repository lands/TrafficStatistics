# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/opt/works/git/TrafficStatistics/common/SQLiteCpp/sqlite3/sqlite3.c" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/common/SQLiteCpp/sqlite3/sqlite3.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "common/sand"
  "common/jsoncpp/json"
  "common/SQLiteCpp/sqlite3"
  "common/SQLiteCpp/include"
  "./include"
  "."
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/opt/works/git/TrafficStatistics/common/SQLiteCpp/src/Backup.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/common/SQLiteCpp/src/Backup.cpp.o"
  "/opt/works/git/TrafficStatistics/common/SQLiteCpp/src/Column.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/common/SQLiteCpp/src/Column.cpp.o"
  "/opt/works/git/TrafficStatistics/common/SQLiteCpp/src/Database.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/common/SQLiteCpp/src/Database.cpp.o"
  "/opt/works/git/TrafficStatistics/common/SQLiteCpp/src/Exception.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/common/SQLiteCpp/src/Exception.cpp.o"
  "/opt/works/git/TrafficStatistics/common/SQLiteCpp/src/Statement.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/common/SQLiteCpp/src/Statement.cpp.o"
  "/opt/works/git/TrafficStatistics/common/SQLiteCpp/src/Transaction.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/common/SQLiteCpp/src/Transaction.cpp.o"
  "/opt/works/git/TrafficStatistics/common/jsoncpp/jsoncpp.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/common/jsoncpp/jsoncpp.cpp.o"
  "/opt/works/git/TrafficStatistics/common/sand/sand.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/common/sand/sand.cpp.o"
  "/opt/works/git/TrafficStatistics/src/traffic_data_process.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/src/traffic_data_process.cpp.o"
  "/opt/works/git/TrafficStatistics/src/traffic_statistics.cpp" "/opt/works/git/TrafficStatistics/CMakeFiles/TrafficStatistics.dir/src/traffic_statistics.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "common/sand"
  "common/jsoncpp/json"
  "common/SQLiteCpp/sqlite3"
  "common/SQLiteCpp/include"
  "./include"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
